use std::error::Error;
use std::path::Iter;
use std::str::CharIndices;


pub struct State {
    pub a: i32,
}

struct Span<'a> (&'a str);

enum Node {
    Path, // Path
    List, // Generalized list
    Pipe, // A pipe
    Namex, // Named expression

}

pub struct Ast<'a> {
    span: Span<'a>,
    node: Node,
}
/*
enum Token<'a> {
    Lexical(&'a str),
    Pipe,
    Name,
    Member,
    ParenOpen,
    ParenClose,
};

struct TokenIter<'a> {
    input: &'a str,
    iter: CharIndices<'a>,
}

impl<'a> Iterator for TokenIter<'a> {
    type Item = Token<'a>;

    fn next(&mut self) -> Option<Token<'a>> {
        self.iter.next().map(|idx, c| {
            use Token;
            match c {
                'a'...'z' => Token::Lexical()
            }
        })
    }
}*/

impl State {
    pub fn parse<'a>(&self, source: &'a str) -> Result<Ast<'a>, Box<dyn Error>> {

        let a = 5;

        let b = 10;

        let c = a + b + self.a;

        dbg!(c);

        Ok(Ast { span: Span(source), node: Node::Path})
    }
}
