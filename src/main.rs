use std::error::Error;
use std::path::Iter;

mod parse;

fn main() -> Result<(), Box<dyn Error>> {
    let source = std::fs::read_to_string("samples/hello.ngr")?;

    let s = parse::State{ a: 99 };

    s.parse(&source)?;
    Ok(())
}